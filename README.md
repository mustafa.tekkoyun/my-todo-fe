# Todo-List Project
This is a simple todo-app project.
You can run this project in two ways.
## 1.Public.
Click for open app: http://178.62.112.186:3000/
## 2.Localhost
This project is a react project. In order for this project to work, npm must be installed on your computer.
### Installation and Setup Steps
Clone down this repository from https://gitlab.com/mustafa.tekkoyun/my-todo-fe . 
#### Installation
**`npm install`**
#### Start Server
**`npm start`**
#### Test
**`npm test`**
#### Browser
click for open **[http://localhost:3000](http://localhost:3000)**

## Test Tool
In this project, enzyme was used for component test and puppeteer for e2e test. TDD approach was tried to be used.