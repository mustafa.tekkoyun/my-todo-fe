import axios from 'axios';
import {baseApi} from '../config/environment';
const getTodoList=()=>{
    return axios.get(baseApi.getBaseApi()+"/api/todos");
}

const createTodoItem=(value)=>{
    return axios.post(baseApi.getBaseApi()+"/api/create-todo", value);
}
export const api ={
    getTodoList,
    createTodoItem
}