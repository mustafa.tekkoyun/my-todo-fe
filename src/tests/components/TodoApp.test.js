import React from 'react';
import { shallow, mount, render } from '../../enzyme'
import TodoApp from '../../components/TodoApp';

describe('TodoApp component renders.', () => {

    it('renders TodoApp Wrapper Component', () => {
        const wrapper=shallow(
            <TodoApp/>
        )
        expect(wrapper.find('#TodoApp').length).toEqual(1);
    });

});