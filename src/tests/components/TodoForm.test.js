import React from 'react';
import { shallow, mount, render } from '../../enzyme'
import TodoForm from '../../components/TodoForm';

let onActionMock = jest.fn();
describe('TodoForm component renders.', () => {

    it('renders input item', () => {
        const wrapper=shallow(
            <TodoForm myAddTodo={onActionMock} onChange={onActionMock} valueText="Test Value"/>
        )
        expect(wrapper.find('#inputText').length).toEqual(1);
    });

    it('renders button item', () => {
        const wrapper=shallow(
            <TodoForm myAddTodo={onActionMock} onChange={onActionMock} valueText="Test Value"/>
        )
        expect(wrapper.find('#addButton').length).toEqual(1);
    });
});