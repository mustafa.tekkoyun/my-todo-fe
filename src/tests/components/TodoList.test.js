import React from 'react';
import { shallow, mount, render } from '../../enzyme'
import TodoList from '../../components/TodoList';

describe('TodoList component renders.', () => {

    it('renders one TodoItem in TodoList', () => {
        const todos=
        [
            {
                id:1, done:false,text:'buy some milk'
            }
        ]

        const wrapper=shallow(
            <TodoList items={todos}/>
        )
        expect(wrapper.find('.todoItem').length).toEqual(1);
    });

    it('renders two TodoItem in TodoList', () => {
        const todos=
        [
            {
                id:1, done:false,text:'buy some milk'
            },
            {
                id:2, done:false,text:'enjoy the assignment'
            }
        ]

        const wrapper=shallow(
            <TodoList items={todos}/>
        )
        expect(wrapper.find('.todoItem').length).toEqual(2);
    });

});