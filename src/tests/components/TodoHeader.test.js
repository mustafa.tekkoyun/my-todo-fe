import React from 'react';
import { shallow, mount, render } from '../../enzyme'
import TodoHeader from '../../components/TodoHeader';

describe('TodoHeader component renders.', () => {

    it('renders header component', () => {
        const wrapper=shallow(
            <TodoHeader />
        )
        expect(wrapper.find('h1#header').length).toEqual(1);
        expect(wrapper.find('h1#header').text()).toEqual("Todo List Project");
    });
});