const faker = require('faker');
const puppeteer = require('puppeteer');
const should = require('chai').should();
let browser;
let page;
beforeAll(async () => {
    // launch browser
    browser = await puppeteer.launch(
        {
            headless: false, 
            slowMo: 50, // type speed
        }
    )
    // new tab
    page = await browser.newPage()
})
describe('adding items to list',()=>{

    test('Should be added items to Todo list',async()=>{
        await page.goto("http://localhost:3000");

        await page.waitForSelector('#TodoApp').then(() => console.log("#TodoApp is ready"));
        await page.waitForSelector('#header').then(() => console.log("#TodoHeader is ready"));
        await page.waitForSelector('#todoForm').then(() => console.log("#todoForm is ready"));
        await page.waitForSelector('#todoListWrapper').then(() => console.log("#todoListWrapper is ready"));
        
        let currentItemCount;
        let todoCount;

        currentItemCount = await page.evaluate(() => document.querySelectorAll('.todoItem').length);
        await page.click("#inputText");
        await page.type("#inputText", "Item 1");
        await page.click("#addButton");
        todoCount = await page.evaluate(() => document.querySelectorAll('.todoItem').length);
        todoCount.should.eq(currentItemCount+1);

        currentItemCount = await page.evaluate(() => document.querySelectorAll('.todoItem').length);
        await page.click("#inputText");
        await page.type("#inputText", "Item 2");
        await page.click("#addButton");
        todoCount = await page.evaluate(() => document.querySelectorAll('.todoItem').length);
        todoCount.should.eq(currentItemCount+1);
        
    },15000)

})
    