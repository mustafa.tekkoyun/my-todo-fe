const LOCAL_API = "localhost"
const DEV_API="178.62.112.186"
const API_PORT="4000"

const HTTP ="http://"
const ENVIRONMENT_TYPES={
    LOCAL:"LOCAL",
    DEV:"DEV"
}

const currentEnvironment = ENVIRONMENT_TYPES.DEV;

const getBaseApi=()=>{
    switch(currentEnvironment){
        case ENVIRONMENT_TYPES.LOCAL:
            return HTTP+LOCAL_API+":"+API_PORT;
        case ENVIRONMENT_TYPES.DEV:
            return HTTP+DEV_API+":"+API_PORT;
        default:
            return HTTP+LOCAL_API+":"+API_PORT;
    }
}

export const baseApi ={
    getBaseApi
}
