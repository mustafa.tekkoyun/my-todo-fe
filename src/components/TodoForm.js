import React from 'react';
class TodoForm extends React.Component{

    _onClick =(e)=>{
        this.props.myAddTodo();
    }

    _onChange = (e) => {
        this.props.onChange(e.target.value);
    }
    render(){
        return(
            <div id="todoForm" className="bg-white mv5 radius5">
                <input id="inputText" onChange={this._onChange} name="text" value={this.props.valueText}></input>
                <button id="addButton" onClick={this._onClick}>Add</button>
            </div>
        );
    }
}

export default TodoForm;