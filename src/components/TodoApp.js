import React from 'react';
import TodoHeader from './TodoHeader';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import {api} from '../api/api';


class TodoApp extends React.Component{
  constructor(props) {
    super(props);
    this.state = { items: [], text: '' };
  }

  getTodos = () => {
    api.getTodoList().then(response=>{
      this.setState({items:response.data.todos});
    })
    
  }
  componentDidMount(){
    this.getTodos();
  }
  updateText = (value) => {
    this.setState({text:value});
  }

  addTodo = () => {
    if(this.state.text.length!==0){
      const todo = {text:this.state.text}
      
      api.createTodoItem(todo).then(response=>{
        const responseTodoItem=response.data.todo;
        this.setState(state => ({
          items: state.items.concat(responseTodoItem)
        }));
      })

      this.updateText("");
    }
  }
  render(){
    return (
        <div id="TodoApp">
          <TodoHeader/>
          <TodoForm myAddTodo={this.addTodo} onChange={this.updateText} valueText={this.state.text}/>
          <TodoList items={this.state.items}/>
        </div>
    );
  }
  
}

export default TodoApp;