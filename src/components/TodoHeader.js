import React from 'react';
class TodoHeader extends React.Component{
    render(){
        return(
            <h1 id="header" className="text-center bg-white radius5 p5 mv5">Todo List Project</h1>
        );
    }
}

export default TodoHeader;