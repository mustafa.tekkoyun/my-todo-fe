import React from 'react';
class TodoList extends React.Component{
    render(){
        return(
            <div id="todoListWrapper" className="radius5">
                    <h1 className="text-center">Todo List [{this.props.items.length}]</h1>
                    {
                    this.props.items.map(item=>(
                        <div key={item.id} className="todoItem">{item.text}</div>
                    ))
                }
            </div>
        );
    }
}

export default TodoList;